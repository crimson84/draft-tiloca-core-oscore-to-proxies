# OSCORE-capable Proxies

This is the working area for the individual Internet-Draft, "OSCORE-capable Proxies".

* [Editor's Copy](https://crimson84.gitlab.io/draft-tiloca-core-oscore-to-proxies)
* [Individual Draft](https://datatracker.ietf.org/doc/html/draft-tiloca-core-oscore-to-proxies)
* [Compare Editor's Copy to Individual Draft](https://https:crimson84.github.io/gitlab.com/#go.draft-tiloca-core-oscore-to-proxies.diff)

## Building the Draft

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

This requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/master/doc/SETUP.md).


## Contributing

See the
[guidelines for contributions](https://github.com/https:/gitlab.com/blob/master/CONTRIBUTING.md).
